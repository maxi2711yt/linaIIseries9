import numpy as np

# 2D Vector
class vec2:
    x: float = 0
    y: float = 0

    def __init__(self, x: float, y: float) -> None:
        self.x = x
        self.y = y

def vec2add(a: vec2, b: vec2) -> vec2:
    return vec2(a.x + b.x, a.y + b.y)

def vec2sub(a: vec2, b: vec2) -> vec2:
    return vec2(a.x - b.x, a.y - b.y)

def vec2dot(a: vec2, b: vec2) -> float:
    return a.x * b.x + a.y * b.y

def vec2norm(vec: vec2) -> float:
    return np.sqrt(vec2dot(vec, vec))

def vec2scale(vec: vec2, scale: float) -> vec2:
    return vec2(vec2.x * scale, vec2.y * scale)

# 3D Vector
class vec3:
    x: float = 0
    y: float = 0
    y: float = 0

    def __init__(self, x: float, y: float, z: float) -> None:
        self.x = x
        self.y = y
        self.z = z

def vec3add(a: vec3, b: vec3) -> vec3:
    return vec3(a.x + b.x, a.y + b.y, a.z + b.z)

def vec3sub(a: vec3, b: vec3) -> vec3:
    return vec3(a.x - b.x, a.y - b.y, a.z - b.z)

def vec3dot(a: vec3, b: vec3) -> float:
    return a.x * b.x + a.y * b.y + a.z * b.z 

def vec3norm(vec: vec3) -> float:
    return np.sqrt(vec3dot(vec, vec))

def vec3scale(vec: vec3, scale: float) -> vec3:
    return vec3( vec.x * scale, vec.y * scale, vec.z * scale)

if __name__ == "__main__":
    print("This file is not intended to be run stand alone!")
