import matplotlib.pyplot as plt
from mpl_toolkits.axisartist.axislines import SubplotZero

import linmath as lm
import numpy as np

def plot(a: lm.vec2, b: lm.vec2 = None, name = 'fig1') -> None:
    # Setting up axes
    fig = plt.figure()
    ax = SubplotZero(fig, 111)
    fig.add_subplot(ax)
    ax.set_aspect('equal')

    # Setting axis
    for dir in ['xzero', 'yzero']:
        ax.axis[dir].set_axisline_style('-|>')
        ax.axis[dir].set_visible(True)
        ax.axis[dir].set_zorder(-1.0)

    # Hiding spines
    for dir in ['left', 'right', 'top', 'bottom']:
        ax.axis[dir].set_visible(False)

    # Setting domains
    delta = max(abs(lm.vec2norm(a)), abs(lm.vec2norm(b)))
    delta = delta + 0.25
    plt.axis([-delta,delta,-delta,delta])
    plt.grid(True, zorder = -2.0)

    # Plotting data
    ax.arrow(0, 0, a.x, a.y, color = 'b', width=0.01, length_includes_head = True, head_width = 0.05)
    ax.arrow(0, 0, b.x, b.y, color = 'r', width=0.01, length_includes_head = True, head_width = 0.05)

    # Exporting files
    plt.savefig("../output/{n}.png".format(n = name), pad_inches = 0.1, bbox_inches = 'tight', dpi = 300)

# D function from the excercise
def D(alpha: float, vec: lm.vec2) -> lm.vec2:
    output = lm.vec2(0, 0)
    output.x = np.cos(alpha) * vec.x - np.sin(alpha) * vec.y
    output.y = np.sin(alpha) * vec.x + np.cos(alpha) * vec.y
    return output

# f function from the excercise
def f(vec: lm.vec2) -> lm.vec2:
    return lm.vec2(vec.x, -vec.y)

# Generates all the plots numbered for a list of vectors 
def generate_plots(raw_vectors: list, rotated_vectors: list, offset: int = 0) -> None:
    if len(raw_vectors) != len(rotated_vectors):
        print("Count of raw_vectors and rotated_vectors are not the same!")
        return

    for i in range(len(raw_vectors)):
        plot(raw_vectors[i], rotated_vectors[i], i + offset)
        print("Generated {index} out of {amount} plots...".format(index = i, amount = len(raw_vectors)))

# Main function
def main() -> None:
    raw_vectors = [lm.vec2(1, 0), lm.vec2(0, 1), lm.vec2(1,1), lm.vec2(-1, 1), lm.vec2(-1, -1)]
    rotated_vectors = []
    flipped_vectors = []
    alpha = 0.25 * np.pi # Angle

    # Calculating the rotated vectors
    for i, vec in enumerate(raw_vectors):
        rotated_vectors.append(D(alpha, vec))
        print("Calculated D_a of {index} out of {amount}...".format(index = i, amount = len(raw_vectors)))         

    for i, vec in enumerate(raw_vectors):
        flipped_vectors.append(f(vec))
        print("Calculated f of {index} out of {amount}...".format(index = i, amount = len(raw_vectors)))


    generate_plots(raw_vectors, rotated_vectors)
    generate_plots(raw_vectors, flipped_vectors, len(rotated_vectors))

if __name__=="__main__":
    main()
